/*
  This is the entry point to the application.
  All that we have to do is to require all needed
  polyfills, setup all configurable libraries (if any),
  and finally run our code

 */

import app from './main'

// this is not really needed, but I still use it, just letting everything else to render
// if it is not the last thing in the body
document.addEventListener('DOMContentLoaded', () => {
    console.log("add first commit");
    console.log("add 2nd commit");
    console.log('place = gorakhpur');
    <p>shivesh : gorakhpur</p>
    function hello() { 
      document.getElementById("demo").innerHTML = "Hello shivesh!";
    }
    hello();
  app()
})
